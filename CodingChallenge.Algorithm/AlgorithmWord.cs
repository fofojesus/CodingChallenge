﻿using CodingChallenge.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodingChallenge.Words
{
    public static class AlgorithmWord
    {
        

        public static string calculate(string word)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbNonLetters = new StringBuilder();
            char[] listOfChar = word.ToCharArray();
            int startPosition = 0;
            int endPosition = 0;
            bool endWord = false;
            
            List<WordDomain> listOfWord = new List<WordDomain>();
            List<CharLetters> listOfCharWords = new List<CharLetters>();
            int totalRecords = listOfChar.Length;
            
            for (int i = 0; i < totalRecords; i++)
            {
                endWord = false;
                
                if (Char.IsLetter(listOfChar[i]))
                {                    
                    var lett = listOfCharWords.Where(x => x.Letter == listOfChar[i]).SingleOrDefault();
                    if(lett is null)
                    {
                        listOfCharWords.Add(new CharLetters
                        {
                             Letter = listOfChar[i],
                             totalLetter = 1
                        });
                    }
                    else
                    {
                        lett.totalLetter += 1;
                    }
                    
                    sb.Append(listOfChar[i].ToString());                   
                }else
                {                    
                    sbNonLetters.Append(listOfChar[i]);
                }


                if (i < totalRecords && i > 0)        
                    {
                        if(Char.IsLetter(listOfChar[i- 1] ) && !Char.IsLetter(listOfChar[i])) endWord = true;
                    if (Char.IsLetter(listOfChar[i - 1]) && Char.IsLetter(listOfChar[i]) && (i == totalRecords -1 )) endWord = true;
                }
                
                if (endWord)
                {

                    string _word = sb.ToString();
                    int sbNumbers = 0;
                    foreach (var number in listOfCharWords)
                    {
                        sbNumbers += 1;
                    }
                    listOfWord.Add(new WordDomain
                    {
                        word = sb.ToString(),
                        startPosition = startPosition,
                        endPosition = endPosition,
                        isWord = true,
                        newWord = _word.Substring(0, 1) + (sbNumbers-2).ToString() + _word.Substring(_word.Length -1 , 1)
                }); ;
                    endWord = false;                    
                    listOfCharWords = new List<CharLetters>();
                     _ = sb.Clear();
                }

                if(i < (totalRecords-1) && i > 0)
                {
                                        
                    if ((!Char.IsLetter(listOfChar[i]) && Char.IsLetter(listOfChar[i + 1])) || (!Char.IsLetter(listOfChar[i]) && i== totalRecords -1 ))
                    {
                     
                        listOfWord.Add(new WordDomain
                        {
                            word = sbNonLetters.ToString(),
                            startPosition = startPosition,
                            endPosition = endPosition,
                            isWord = false
                        }); ;
                                                
                        _ = sbNonLetters.Clear();
                    }
                }

            }
            StringBuilder res =new StringBuilder();
            foreach (var number in listOfWord)
            {
                res.Append(number.newWord) ;
            }
            return res.ToString();
        }

       

    }
}
