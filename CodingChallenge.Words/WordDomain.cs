﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodingChallenge.Words
{
    public class WordDomain
    {
        public string word { get; set; }
        public int startPosition { get; set; }
        public int endPosition { get; set; }
        public bool isWord { get; set; }
        public string newWord { get; set; }
    }
}
